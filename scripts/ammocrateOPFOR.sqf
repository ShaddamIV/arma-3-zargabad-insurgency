while {alive _this} do{

//Settings 
_weaponAmount = 5;
_magAmount = 100;
_itemAmount = 15;
_sleepAmount = 1800;

_this allowDamage false;

//Clear Box
clearweaponcargoGlobal _this;
clearmagazinecargoGlobal _this;
clearItemcargoGlobal _this;
clearBackpackCargo _this;


_this additemcargoGlobal ["V_BandollierB_khk", _itemAmount];
_this additemcargoGlobal ["V_Chestrig_khk", _itemAmount];
_this additemcargoGlobal ["optic_mas_pso_c", _itemAmount];
_this additemcargoGlobal ["optic_mas_acog_rd_c", _itemAmount];


//_this addweaponcargoGlobal ["Laserdesignator", _itemAmount];
_this addweaponcargoGlobal ["Binocular", _itemAmount];
_this addweaponcargoGlobal ["arifle_mas_aks74u_c", _weaponAmount];
_this addweaponcargoGlobal ["LMG_mas_rpk_F_h", _weaponAmount];
_this addweaponcargoGlobal ["LMG_mas_pkm_F_h", _weaponAmount];
_this addweaponcargoGlobal ["arifle_mas_ak_74m", _weaponAmount];
_this addweaponcargoGlobal ["arifle_mas_ak_74m_gl_c", _weaponAmount];
_this addweaponcargoGlobal ["arifle_mas_ak_74m_sf_gl_c", _weaponAmount];
_this addweaponcargoGlobal ["arifle_mas_ak_74m_sf_c", _weaponAmount];
_this addweaponcargoGlobal ["arifle_mas_akms_gl_c", _weaponAmount];
_this addweaponcargoGlobal ["LMG_mas_m72_F", _weaponAmount];
_this addweaponcargoGlobal ["srifle_mas_svd", _weaponAmount];
_this addweaponcargoGlobal ["CAF_PKM", _weaponAmount];
_this addweaponcargoGlobal ["launch_RPG32_F", _weaponAmount];


_this addmagazinecargoGlobal ["30Rnd_mas_545x39_mag", _magAmount];
_this addmagazinecargoGlobal ["30Rnd_mas_545x39_T_mag", _magAmount];
_this addmagazinecargoGlobal ["1Rnd_HE_Grenade_shell", _magAmount];
_this addmagazinecargoGlobal ["30Rnd_mas_762x39_mag", _magAmount];
_this addmagazinecargoGlobal ["100Rnd_mas_762x39_mag", _magAmount];
_this addmagazinecargoGlobal ["10Rnd_mas_762x54_mag", _magAmount];
_this addmagazinecargoGlobal ["100Rnd_mas_545x39_mag", _magAmount];
_this addmagazinecargoGlobal ["100Rnd_mas_762x54_mag", _magAmount];
_this addmagazinecargoGlobal ["CAF_100RND_762x54_PKM", _magAmount];
_this addmagazinecargoGlobal ["RPG32_HE_F", _magAmount];
_this addmagazinecargoGlobal ["RPG32_F", _magAmount];
_this addmagazinecargoGlobal ["HandGrenade", _magAmount];
_this addmagazinecargoGlobal ["MiniGrenade", _magAmount];
_this addmagazinecargoGlobal ["Chemlight_green", _magAmount];
_this addmagazinecargoGlobal ["Chemlight_red", _magAmount];
_this addmagazinecargoGlobal ["Chemlight_yellow", _magAmount];
_this addmagazinecargoGlobal ["Chemlight_blue", _magAmount];

sleep _sleepAmount;

};