while {alive _this} do{

//Settings 
_weaponAmount = 5;
_magAmount = 100;
_itemAmount = 2;
_sleepAmount = 1800;

_this allowDamage false;

//Clear Box
clearweaponcargoGlobal _this;
clearmagazinecargoGlobal _this;
clearItemcargoGlobal _this;
clearBackpackCargo _this;


_this additemcargoGlobal ["U_Afghan01", _itemAmount];
_this additemcargoGlobal ["U_Afghan01NH", _itemAmount];
_this additemcargoGlobal ["U_Afghan02", _itemAmount];
_this additemcargoGlobal ["U_Afghan02NH", _itemAmount];
_this additemcargoGlobal ["U_Afghan03", _itemAmount];
_this additemcargoGlobal ["U_Afghan03NH", _itemAmount];
_this additemcargoGlobal ["U_Afghan04", _itemAmount];
_this additemcargoGlobal ["U_Afghan05", _itemAmount];
_this additemcargoGlobal ["U_Afghan06", _itemAmount];
_this additemcargoGlobal ["U_Afghan06NH", _itemAmount];
_this additemcargoGlobal ["Afghan_01Hat", _itemAmount];
_this additemcargoGlobal ["Afghan_02Hat", _itemAmount];
_this additemcargoGlobal ["Afghan_03Hat", _itemAmount];
_this additemcargoGlobal ["Afghan_04Hat", _itemAmount];
_this additemcargoGlobal ["Afghan_05Hat", _itemAmount];
_this additemcargoGlobal ["Afghan_06Hat", _itemAmount];
_this additemcargoGlobal ["U_CAF_AG_ME_ROBES_01", _itemAmount];
_this additemcargoGlobal ["U_CAF_AG_ME_ROBES_01a", _itemAmount];
_this additemcargoGlobal ["U_CAF_AG_ME_ROBES_01b", _itemAmount];
_this additemcargoGlobal ["U_CAF_AG_ME_ROBES_01c", _itemAmount];
_this additemcargoGlobal ["U_CAF_AG_ME_ROBES_01d", _itemAmount];
_this additemcargoGlobal ["U_CAF_AG_ME_ROBES_02", _itemAmount];
_this additemcargoGlobal ["U_CAF_AG_ME_ROBES_02a", _itemAmount];
_this additemcargoGlobal ["U_CAF_AG_ME_ROBES_02b", _itemAmount];
_this additemcargoGlobal ["U_CAF_AG_ME_ROBES_02c", _itemAmount];
_this additemcargoGlobal ["U_CAF_AG_ME_ROBES_02d", _itemAmount];
_this additemcargoGlobal ["U_CAF_AG_ME_ROBES_03", _itemAmount];
_this additemcargoGlobal ["U_CAF_AG_ME_ROBES_03a", _itemAmount];
_this additemcargoGlobal ["U_CAF_AG_ME_ROBES_03b", _itemAmount];
_this additemcargoGlobal ["U_CAF_AG_ME_ROBES_03c", _itemAmount];
_this additemcargoGlobal ["U_CAF_AG_ME_ROBES_03d", _itemAmount];
_this additemcargoGlobal ["U_CAF_AG_ME_ROBES_04", _itemAmount];
_this additemcargoGlobal ["U_CAF_AG_ME_ROBES_04a", _itemAmount];
_this additemcargoGlobal ["U_CAF_AG_ME_ROBES_04b", _itemAmount];
_this additemcargoGlobal ["U_CAF_AG_ME_ROBES_04c", _itemAmount];
_this additemcargoGlobal ["U_CAF_AG_ME_ROBES_04d", _itemAmount];
_this additemcargoGlobal ["U_CAF_AG_ME_ROBES_mil_01", _itemAmount];
_this additemcargoGlobal ["U_CAF_AG_ME_ROBES_mil_01a", _itemAmount];
_this additemcargoGlobal ["H_CAF_AG_TURBAN", _itemAmount];
_this additemcargoGlobal ["H_CAF_AG_PAKTOL", _itemAmount];
_this additemcargoGlobal ["H_CAF_AG_PAKTOL_01", _itemAmount];
_this additemcargoGlobal ["H_CAF_AG_PAKTOL_02", _itemAmount];
_this additemcargoGlobal ["H_CAF_AG_PAKTOL_03", _itemAmount];
_this additemcargoGlobal ["H_CAF_AG_WRAP", _itemAmount];
_this addBackpackCargoGlobal ["B_AssaultPack_dgtl", _itemAmount];



sleep _sleepAmount;

};