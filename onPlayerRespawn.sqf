PlayerMedic = ["B_medic_F","B_recon_medic_F","B_mas_mar_medic_F_v","B_mas_mar_medic_F_d","B_mas_mar_medic_F_rec_d","B_mas_mar_medic_F_rec_v","B_mas_mar_medic_F_rec_vn"];// can build sandbag
player setVariable ["BIS_noCoreConversations", true];
enableSentences false;
player disableConversation true;
player setSpeaker "NoVoice";

if((!isClass(configFile >> "cfgPatches" >> "ace_common")) &&
	{!isClass(configFile >> "cfgPatches" >> "DRI_nofatigue")}) then {
		[] execVM 'common\client\Fatigue.sqf';};

if(player == P1) then {
		null = [P1, 700, true, -1] execVM "common\client\smoke.sqf";} else {};
		
if(player == P4) then {
		null = [P4, 700, true, -1] execVM "common\client\CAS\addAction.sqf";} else {};
	
if(player == P5) then {
		P5 removeWeapon "MMG_02_sand_RCO_LP_F"; P9 addWeapon "MMG_02_black_F";} else {};
	
if(player == P9) then {
		P9 removeWeapon "MMG_02_sand_RCO_LP_F"; P9 addWeapon "MMG_02_black_F";} else {};

if(player == P17) then {
		P17 removeWeapon "MMG_02_sand_RCO_LP_F"; P17 addWeapon "MMG_02_black_F";} else {};

private "_playertype";
	_playertype = typeOf (vehicle player);
	// Medic
	if (_playertype in PlayerMedic) then {player addAction ["Place Sandbag", {call SiC_fnc_placeSandBag}];}; 
//if (_playertype in PlayerMedic) then {_id = player addAction ["Place Sandbag", {call SiC_fnc_placeSandBag}]}; 

		
		if ((!isServer) && (player != player)) then
{
waitUntil {player == player};
};

player enableFatigue false;

if (side player isequalto opfor) then {
            //debug to see where box spawned is if not multiplayer
            _m = createMarker [format ["box%1",random 1000],getposATL cache];
            _m setMarkerShape "ICON"; 
            _m setMarkerType "hd_objective";
            _m setMarkerColor "ColorRed";
		};  