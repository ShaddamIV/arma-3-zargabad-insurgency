while {alive _this} do{

//Settings 
_weaponAmount = 5;
_magAmount = 25;
_itemAmount = 3;
_sleepAmount = 30000;

_this allowDamage false;

//Clear Box
clearweaponcargoGlobal _this;
clearmagazinecargoGlobal _this;
clearItemcargoGlobal _this;
clearBackpackCargo _this;



_this additemcargoGlobal ["SMA_BARSKA", _itemAmount];
//_this additemcargoGlobal ["optic_LRPS", _itemAmount];
//_this additemcargoGlobal ["", 2];

//_this addweaponcargoGlobal ["Laserdesignator", _itemAmount];
_this addweaponcargoGlobal ["Binocular", _itemAmount];
_this addweaponcargoGlobal ["srifle_mas_svd_h", _weaponAmount];
_this addweaponcargoGlobal ["LMG_mas_pkm_F", _weaponAmount];
_this addweaponcargoGlobal ["arifle_mas_g3_a", _weaponAmount];
_this addweaponcargoGlobal ["mas_launch_Stinger_F", _weaponAmount];
_this addweaponcargoGlobal ["launch_RPG32_F", _weaponAmount];


_this addmagazinecargoGlobal ["10Rnd_mas_762x54_T_mag", _magAmount];
_this addmagazinecargoGlobal ["100Rnd_mas_762x54_mag", _magAmount];
_this addmagazinecargoGlobal ["RPG32_HE_F", _magAmount];
_this addmagazinecargoGlobal ["RPG32_F", _magAmount];
_this addmagazinecargoGlobal ["20Rnd_762x51_Mag", _magAmount];
_this addmagazinecargoGlobal ["mas_Stinger", _magAmount];
_this addmagazinecargoGlobal ["HandGrenade", _magAmount];
_this addmagazinecargoGlobal ["MiniGrenade", _magAmount];
_this addmagazinecargoGlobal ["Chemlight_green", _magAmount];
_this addmagazinecargoGlobal ["Chemlight_red", _magAmount];
_this addmagazinecargoGlobal ["Chemlight_yellow", _magAmount];
_this addmagazinecargoGlobal ["Chemlight_blue", _magAmount];

sleep _sleepAmount;

};