while {alive _this} do{

//Settings 
_sleepAmount = 1800;

_this allowDamage false;

//Clear Box
clearweaponcargoGlobal _this;
clearmagazinecargoGlobal _this;
clearItemcargoGlobal _this;
clearBackpackCargo _this;

_this additemcargoGlobal ["FirstAidKit", 100];
_this additemcargoGlobal ["Medikit", 5];

sleep _sleepAmount;

};