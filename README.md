This is a direct port of the Insurgency type mission from Takistan to Zargabad.

Aim of this project is to have the same mission,minus some not-so-useful features and scripts,on the Zargabad map to switch from a wide open map to a more closed enviroment with
many houses and corners instead of hills. Its one big city with smaller outskirts.

Also,this serves as "best practice" project to port the mission to basically every map that people would like AND to have a working framework to create a similiar scenario on the upcoming official Map. That project will involve Insurgency type gameplay against Narcos and involve drugs/drug processing plants instead of smuggled weapon caches.

Modpack is the same as for Takistan Insurgency