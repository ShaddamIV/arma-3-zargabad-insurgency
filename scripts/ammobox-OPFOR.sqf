while {alive _this} do{

//Settings 
_weaponAmount = 5;
_magAmount = 100;
_itemAmount = 15;
_sleepAmount = 1800;

_this allowDamage false;

//Clear Box
clearweaponcargoGlobal _this;
clearmagazinecargoGlobal _this;
clearItemcargoGlobal _this;
clearBackpackCargo _this;



_this additemcargoGlobal ["hlc_VOG25_AK", _itemAmount];
_this additemcargoGlobal ["hlc_GRD_White", _itemAmount];
_this additemcargoGlobal ["optic_Hamr", _itemAmount];
_this additemcargoGlobal ["HLC_Optic_PSO1", _itemAmount];
_this additemcargoGlobal ["HLC_Optic_1p29", _itemAmount];
_this additemcargoGlobal ["hlc_optic_kobra", _itemAmount];

//_this additemcargoGlobal ["", 2];

//_this addweaponcargoGlobal ["Laserdesignator", _itemAmount];
_this addweaponcargoGlobal ["Binocular", _itemAmount];
_this addweaponcargoGlobal ["hlc_rifle_ak74", _weaponAmount];
_this addweaponcargoGlobal ["hlc_rifle_aks74", _weaponAmount];
_this addweaponcargoGlobal ["hlc_rifle_aks74u", _weaponAmount];
_this addweaponcargoGlobal ["hlc_rifle_ak47", _weaponAmount];
_this addweaponcargoGlobal ["hlc_rifle_akm", _weaponAmount];
_this addweaponcargoGlobal ["hlc_rifle_rpk", _weaponAmount];
_this addweaponcargoGlobal ["hlc_rifle_ak12", _weaponAmount];
_this addweaponcargoGlobal ["hlc_rifle_akmgl", _weaponAmount];
_this addweaponcargoGlobal ["hlc_rifle_aks74_GL", _weaponAmount];
_this addweaponcargoGlobal ["launch_RPG32_F", _weaponAmount];
_this addweaponcargoGlobal ["hgun_mas_mak_F", _weaponAmount];

_this addmagazinecargoGlobal ["hlc_30Rnd_545x39_B_AK", _magAmount];
_this addmagazinecargoGlobal ["hlc_30Rnd_545x39_T_AK", _magAmount];
_this addmagazinecargoGlobal ["hlc_30Rnd_545x39_EP_AK", _magAmount];
_this addmagazinecargoGlobal ["hlc_45Rnd_545x39_t_rpk", _magAmount];
_this addmagazinecargoGlobal ["hlc_30Rnd_762x39_b_ak", _magAmount];
_this addmagazinecargoGlobal ["hlc_30Rnd_762x39_t_ak", _magAmount];
_this addmagazinecargoGlobal ["hlc_45Rnd_762x39_t_rpk", _magAmount];
_this addmagazinecargoGlobal ["hlc_45Rnd_762x39_m_rpk", _magAmount];
_this addmagazinecargoGlobal ["RPG32_HE_F", _magAmount];
_this addmagazinecargoGlobal ["RPG32_F", _magAmount];
_this addmagazinecargoGlobal ["HandGrenade", _magAmount];
_this addmagazinecargoGlobal ["MiniGrenade", _magAmount];
_this addmagazinecargoGlobal ["Chemlight_green", _magAmount];
_this addmagazinecargoGlobal ["Chemlight_red", _magAmount];
_this addmagazinecargoGlobal ["Chemlight_yellow", _magAmount];
_this addmagazinecargoGlobal ["Chemlight_blue", _magAmount];
_this addmagazinecargoGlobal ["8Rnd_mas_9x18_Mag", _magAmount];

sleep _sleepAmount;

};